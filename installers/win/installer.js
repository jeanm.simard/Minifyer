const createWindowsInstaller = require('electron-winstaller').createWindowsInstaller
const path = require('path')
const URL = require('url');

getInstallerConfig()
  .then(createWindowsInstaller)
  .catch((error) => {
    console.error(error.message || error)
    process.exit(1)
  })

function getInstallerConfig () {
  console.log('creating windows installer')
  const rootPath = path.join('./')
  const outPath = path.join(rootPath, 'release-builds')
  return Promise.resolve({
    appDirectory: path.resolve('./Minifyer-win32-x64/'),
    authors: 'TimMatane',
    noMsi: true,
    outputDirectory: path.join(rootPath,'installers/win'),
    exe: 'Minifyer.exe',
    setupExe: 'MinifyerInstaller.exe',
    setupIcon: path.join(rootPath, 'assets', 'icons', 'win', 'icon.ico'),
    iconUrl:URL.format({pathname: path.resolve(path.join(rootPath, 'assets', 'icons', 'win', 'icon.ico')),protocol: 'file:',slashes: true})
  })
}