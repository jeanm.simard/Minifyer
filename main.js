const isWin = process.platform === "win32";


/**
 * *******************************************************
 * Installer Squirrel Events
 * *******************************************************
 */
if (isWin) {
  const setupEvents = require('./installers/win/setupEvents.js')
  if (setupEvents.handleSquirrelEvent()) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
  }
}



/**
 * *******************************************************
 * Modules request
 * *******************************************************
 */
const Path = require('path');
const FS = require('fs');
const URL = require('url')
const LocalStorage = require("node-localstorage").LocalStorage;
const localStorage = new LocalStorage('./scratch');
const Electron = require('electron');
const BrowserWindow = Electron.BrowserWindow
const App = Electron.app;
const Menu = Electron.Menu;
const Dialog = Electron.dialog;
const IPC = Electron.ipcMain;
const isDev = require('electron-is-dev');
if (isDev) {
  require('electron-reload')(__dirname);
}
const chokidar = require("chokidar");
const utils = require('./assets/js/utils.js');
const electronUtils = require('./assets/js/electron-utils.js');
const config = require('./assets/js/config.js').config;
const compressionUtils = require("./assets/js/compression.js");
let currentConfig = electronUtils.createCurrentConfig(config);
const currentLang = currentConfig.general.lang;
const localization = require("./assets/js/localization/" + currentLang);
/**************************************/

/**
 * *******************************************************
 * Init variables
 * *******************************************************
 */


let mainMenu;
let mainWindow, aboutWindow, optionsWindow;
const minDelayBetweenMinification = 1500;
const loadDelay = 1000;
/* Directories watching */
let currentDirectoryID = null;
let watcher;
let time = 0;

let cmd = process.argv[1];
let firstRun = (isDev) ? true : false;
if (cmd == '--squirrel-firstrun') {
  firstRun = true;
} else if (electronUtils.readCurrentConfig().log.firstRun == true) {
  firstRun = true;
  currentConfig.log.firstRun = false;
  electronUtils.saveConfig(currentConfig);
}





/**
 * *******************************************************
 * Dialogs options
 * *******************************************************
 */
const openDialogOptions = {
  filters: [{
    name: "Code file",
    extensions: ["js", "css", "scss"]
  }],
  properties: ['openFile'],
  title: localization.dialogs.OPEN
};

const openConcatDialogOptions = {
  filters: [{
    name: "Code file",
    extensions: ["concat"]
  }],
  properties: ['openFile'],
  title: localization.dialogs.OPEN_CONCAT
};

const openDirectoryOptions = {
  properties: ['openDirectory'],
  title: localization.dialogs.OPEN_DIR
}

const openInputDirectoryOptions = {
  properties: ['openDirectory'],
  title: localization.dialogs.OPEN_INPUT_DIR
}
const openOutputDirectoryOptions = {
  properties: ['openDirectory'],
  title: localization.dialogs.OPEN_OUTPUT_DIR
}

/**
 * *******************************************************
 * Main menu template
 * *******************************************************
 */
const mainMenuTemplate = [
  {
    label: localization.menu.ACTIONS,
    submenu: [
      {
        label: localization.menu.ACTIONS_COMPRESS,
        click: () => {
          Dialog.showOpenDialog(openDialogOptions, onFileOpen);
        },
        accelerator: "CommandOrControl+W"
      },
      {
        label: localization.menu.ACTIONS_CONCAT,
        click: () => {
          Dialog.showOpenDialog(openConcatDialogOptions, onConcatFileOpen);
        },
        accelerator: "CommandOrControl+M"
      },
      {
        label: localization.menu.ACTIONS_WATCH,
        click: () => {
          Dialog.showOpenDialog(openDirectoryOptions, onDirectoryWatch);
        },
        accelerator: "CommandOrControl+D"
      },
      {
        label: localization.menu.ACTIONS_QUIT,
        click: () => {
          App.quit();
        },
        role: "quit",
        accelerator: "CommandOrControl+Q"
      }
    ]
  },
  {
    label: localization.menu.PARAMS,


    submenu: [
      {
        label: localization.menu.PARAMS_OPTIONS,
        id: "options",
        click: () => {
          optionsWindow = electronUtils.newWindow({ width: 600, height: 600, maximizable: false, resizable: false, icon: Path.join(__dirname, 'assets/icons/png/16x16.png'), parent: mainWindow, darkTheme: true, show: false }, Path.join(__dirname, 'pages/options.html'), () => {
            optionsWindow.webContents.send('update-compression-from-config', utils.lazyObjectCopy(currentConfig));
            optionsWindow.show();
          });

          optionsWindow.on('closed', function () {
            mainMenu.getMenuItemById('options').enabled = true;
            optionsWindow = undefined;
          })
          mainMenu.getMenuItemById('options').enabled = false;
        },
        accelerator: "CommandOrControl+O"
      },
      {
        label: localization.menu.PARAMS_DEV_TOOLS,
        visible: isDev,
        click: () => {
          mainWindow.webContents.openDevTools()
          if (optionsWindow) {
            optionsWindow.webContents.openDevTools()
          }
        }
      }
    ]

  },
  {
    label: localization.menu.ABOUT,
    submenu: [
      {
        label: localization.menu.ABOUT_DOC,
        id: 'about',
        click: () => {
          aboutWindow = electronUtils.newWindow({ width: 600, height: 500, maximizable: false, resizable: false, icon: Path.join(__dirname, 'assets/icons/png/16x16.png'), parent: mainWindow, darkTheme: true, show: false }, Path.join(__dirname, 'pages/about-' + currentLang + '.html'), () => {
            aboutWindow.show();
          });
          aboutWindow.on('closed', function () {
            mainMenu.getMenuItemById('about').enabled = true;
            aboutWindow = undefined;
          })
          mainMenu.getMenuItemById('about').enabled = false;
          // aboutWindow.webContents.openDevTools()
        },
        accelerator: "CommandOrControl+I"
      }, {
        type: "separator"
      }, {
        label: localization.menu.ABOUT_VERSION + " " + App.getVersion(),
        enabled: false
      }, {
        label: "TIM Matane 2018",
        enabled: false
      }
    ]
  }
]

mainMenu = Menu.buildFromTemplate(mainMenuTemplate)

/**
 * *******************************************************
 * Main window creation
 * *******************************************************
 */
function createMainWindow() {
  //osx menu
  Menu.setApplicationMenu(mainMenu);
  mainWindow = electronUtils.newWindow({ width: 1024, height: 768, minWidth: 655, minHeight: 480, maxWidth: 1024, maxHeight: 768, maximizable: false, resizable: true, icon: Path.join(__dirname, 'assets/icons/png/16x16.png'), show: false }, Path.join(__dirname, './index.html'), () => {
    listWatchedDirectories();
    if (firstRun) {
      mainWindow.webContents.send('first-run');
      setTimeout(() => {
        mainWindow.show();
        setTimeout(() => {
          mainWindow.webContents.send('loading-done');
        }, loadDelay);
      }, 1000)
    } else mainWindow.show();
  });

  if (isWin) {
    //linux and windows menu
    mainWindow.setMenu(mainMenu);
  }

  App.setAppUserModelId('com.cgmatane.tim.minfiyer');
}
/**
 * *******************************************************
 * Make single instance app
 * *******************************************************
 */

let shouldQuit = App.makeSingleInstance(function (commandLine, workingDirectory) {
  // Someone tried to run a second instance, we should focus our window.
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
});

if (shouldQuit) {
  App.quit();
  return;
}

/**
 * *******************************************************
 * Application Events
 * *******************************************************
 */
App.on('ready', createMainWindow)

// Quit when all windows are closed.
App.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  mainWindow = null;
  if (process.platform !== 'darwin') {
    App.quit()
    if (watcher) {
      watcher.close();
    }
  }
})

App.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createMainWindow();
  }
})

App.on('browser-window-created', function (e, window) {
  window.setMenu(null);
});

/**
 * *******************************************************
 * Dialog Events functions
 * *******************************************************
 */

/**
 * Callback when a file is opened
 * @param {String[]} fileNames 
 */
function onFileOpen(fileNames) {
  if (fileNames === undefined) {
    console.log(localization.messages.NO_FILE_SELECTED);
    return;
  }

  let parsed = Path.parse(fileNames[0]);
  parseChangedFile(parsed, undefined, true);
}

/**
 * Callback when a file is opened for concatenation
 * @param {String[]} fileNames 
 */
function onConcatFileOpen(fileNames) {
  if (fileNames === undefined) {
    console.log(localization.messages.NO_FILE_SELECTED);
    return;
  }

  let parsed = Path.parse(fileNames[0]);
  parseChangedFile(parsed, undefined, true);
}

function onInputDirectoryChange(path) {

  if (path) {
    let dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
    let id = -1;
    for (let index = 0; index < dirs.length; index++) {
      if (dirs[index].id === currentDirectoryID) {
        dirs[index].input = path;
        id = dirs[index].id;
        break;
      }
    }
    localStorage.setItem("watchedDirectories", JSON.stringify(dirs));
    watch(dirs);
    mainWindow.webContents.send('change-watched-input-directory-done', path, id);
  } else {
    console.log(localization.messages.NO_PATH_SELECTED);
  }
  currentDirectoryID = null;
}

function onOutputDirectoryChange(path) {
  //console.log(path, currentDirectoryID);
  let oldOutputPath;
  if (path) {
    let dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
    let id = -1;
    for (let index = 0; index < dirs.length; index++) {
      if (dirs[index].id == currentDirectoryID) {
        oldOutputPath = dirs[index].output;
        dirs[index].output = path;
        id = dirs[index].id;
        break;
      }
    }
    localStorage.setItem("watchedDirectories", JSON.stringify(dirs));
    watch(dirs);
    mainWindow.webContents.send('change-watched-output-directory-done', path, id);
  } else {
    console.log(localization.messages.NO_PATH_SELECTED);
  }
  currentDirectoryID = null;
}


function onDirectoryWatch(path) {
  if (path) {
    let dirs, directoryObject;
    if (localStorage.getItem("watchedDirectories") == null) {
      dirs = [];
      for (let index = 0; index < path.length; index++) {
        directoryObject = { input: path[index], output: path[index], lastModificationDate: utils.theTime(), id: "WD" + (dirs.length + 1) };
        dirs.push(directoryObject);
        mainWindow.webContents.send('new-watched-directory', directoryObject);
      }
      localStorage.setItem("watchedDirectories", JSON.stringify(dirs))
    } else {
      dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
      let dirAlreadyWatched = false;


      let inputs = [];
      for (let index = 0; index < dirs.length; index++) {
        let element = dirs[index];
        inputs.push(element.input)
      }
      for (let index = 0; index < path.length; index++) {
        if (inputs.indexOf(path[index]) == -1) {
          directoryObject = { input: path[index], output: path[index], lastModificationDate: utils.theTime(), id: "WD" + (dirs.length + 1) };
          dirs.push(directoryObject);
          mainWindow.webContents.send('new-watched-directory', directoryObject);
        } else {
          Dialog.showMessageBox({
            type: "info", message: utils.formatString(localization.messages.DIR_ALREADY_WATCHED, { "%DIR%": path[index] }),
            icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
          });
        }
      }


      // if (!dirAlreadyWatched) {
      localStorage.setItem("watchedDirectories", JSON.stringify(dirs))
      // } else {
      //  Dialog.showMessageBox({ type: "info", message: utils.formatString(localization.messages.DIR_ALREADY_WATCHED, { "%DIR%": path[0] }) });
      // }
    }
    watch(dirs);

  } else {
    console.log(localization.messages.NO_PATH_SELECTED);
  }
}

function parseChangedFile(parsedInputDirectory, directoryData, showMessage) {
  showMessage = (showMessage === undefined) ? true : showMessage;
  outputDirectory = (directoryData === undefined) ? parsedInputDirectory.dir : directoryData.output;
  let inputPath = parsedInputDirectory.dir + ((isWin) ? '\\' : '/') + parsedInputDirectory.base;
  if (directoryData != undefined) {
    mainWindow.webContents.send('show-loading', directoryData.id)
  }

  switch (parsedInputDirectory.ext) {
    case ".js":
      let outputPathJS = outputDirectory + ((isWin) ? '\\' : '/') + parsedInputDirectory.name + '.min.js';
      compressionUtils.minifyJS(utils.lazyObjectCopy(currentConfig.js.compression), inputPath, outputPathJS, (err, min) => {
        if (err == null && showMessage) {
          Dialog.showMessageBox({
            type: "info", message: localization.messages.MINIFY_SUCCESS + " " + outputPathJS,
            icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
          });
        } else if (err == null && !showMessage) {
          console.log(localization.messages.MINIFY_SUCCESS + " " + outputPathJS);
          let currTime = utils.theTime();
          electronUtils.updateLocalStorageWatchedDirectories(parsedInputDirectory.dir, 'lastModificationDate', currTime);
          mainWindow.webContents.send('new-compression', currTime, directoryData.id, outputPathJS);
        } else {
          Dialog.showErrorBox(localization.messages.MINIFY_ERROR, err.formatted);
          console.log(localization.messages.MINIFY_ERROR, err.formatted);
        }
      });
      break;
    case '.css':
      let outputPathCSS = outputDirectory + ((isWin) ? '\\' : '/') + parsedInputDirectory.name + '.min.css';
      compressionUtils.minifyCSS(utils.lazyObjectCopy(currentConfig.css.compression), inputPath, outputPathCSS, (err, min) => {
        if (err == null && showMessage) {
          Dialog.showMessageBox({
            type: "info", message: localization.messages.MINIFY_SUCCESS + " " + outputPathCSS,
            icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
          });
        } else if (err == null && !showMessage) {
          console.log(localization.messages.MINIFY_SUCCESS + " " + outputPathCSS);
          let currTime = utils.theTime();
          electronUtils.updateLocalStorageWatchedDirectories(parsedInputDirectory.dir, 'lastModificationDate', currTime);
          mainWindow.webContents.send('new-compression', currTime, directoryData.id, outputPathCSS);
        } else {
          Dialog.showErrorBox(localization.messages.MINIFY_ERROR, err.formatted);
          console.log(localization.messages.MINIFY_ERROR, err.formatted);
        }
      });
      break;
    case '.scss':
      let cssExt = (currentConfig.scss.compression.options.outputStyle == "compressed") ? ".min.css" : ".css";
      let outputPathSCSS = outputDirectory + ((isWin) ? '\\' : '/') + parsedInputDirectory.name + cssExt;
      compressionUtils.sassify(utils.lazyObjectCopy(currentConfig.scss.compression), inputPath, outputPathSCSS, (err, result) => {

        if (!err) {
          FS.writeFile(outputPathSCSS, result.css, function (err) {
            if (!err) {
              if (showMessage) {
                Dialog.showMessageBox({
                  type: "info", message: localization.messages.SASS_WRITE_SUCCESS + " " + outputPathSCSS,
                  icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
                });
              } else {
                console.log(localization.messages.SASS_WRITE_SUCCESS + " " + outputPathSCSS);
                let currTime = utils.theTime();
                mainWindow.webContents.send('new-compression', currTime, directoryData.id, outputPathSCSS);
              }
            } else {
              Dialog.showErrorBox(localization.messages.SASS_WRITE_ERROR, err.formatted);
              console.log(localization.messages.SASS_WRITE_ERROR, err.formatted);
            }
          });
        } else {
          Dialog.showErrorBox(localization.messages.SASS_ERROR, err.formatted);
          console.log(localization.messages.SASS_ERROR, err.formatted);
        }
      });
      break;
    case '.concat':
      FS.readFile(inputPath, 'utf8', function (err, data) {
        if (err) {
          Dialog.showErrorBox(localization.messages.CONCAT_READ_ERROR, err.formatted);
          console.log(localization.messages.CONCAT_READ_ERROR, err.formatted);
          return;
        }

        let inputFiles = data.split(/\r?\n/g);
        let realInputFiles = [];
        let outputFile = parsedInputDirectory.dir + ((isWin) ? '\\' : '/');
        let lastExt = "";
        let outSpecified = false;
        let outRegex = /@out.([A-Za-z0-9\.\_]+)/g;

        let len = inputFiles.length;
        for (let index = 0; index < len; index++) {
          if (inputFiles[index] != undefined) {
            if (inputFiles[index].match(/@out/)) {
              outputFile += outRegex.exec(inputFiles[index])[1];
              outSpecified = true;
            } else if (inputFiles[index] != "") {
              realInputFiles.push(inputFiles[index]);
            }
          }
        }

        if (!outSpecified) {
          for (let index = 0; index < inputFiles.length; index++) {
            outputFile += Path.parse(inputFiles[index]).name + ".";
            lastExt = Path.extname(inputFiles[index]);
          }
          outputFile += "concat" + lastExt;
        }

        for (let index = 0; index < realInputFiles.length; index++) {
          realInputFiles[index] = parsedInputDirectory.dir + ((isWin) ? '\\' : '/') + realInputFiles[index];
        }

        compressionUtils.concat(realInputFiles, outputFile, function (err, min) {
          if (err == null && showMessage) {
            Dialog.showMessageBox({
              type: "info", message: localization.messages.CONCAT_SUCCESS + " " + outputFile,
              icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
            });
          } else if (err == null && !showMessage) {
            console.log(localization.messages.CONCAT_SUCCESS + " " + outputFile);
            let currTime = utils.theTime();
            mainWindow.webContents.send('new-compression', currTime, directoryData.id, outputFile);
          } else {
            Dialog.showErrorBox(localization.messages.CONCAT_ERROR, err.formatted);
            console.log(localization.messages.CONCAT_ERROR, err.formatted);
          }
        });
      })

      break
  }

}
/**
 * *******************************************************
 * IPC Event callbacks
 * *******************************************************
 */

IPC.on('remove-watched-directory', (event, arg) => {
  let dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
  dirs.splice(dirs.indexOf(arg), 1);
  watch(dirs);
  localStorage.setItem("watchedDirectories", JSON.stringify(dirs));
})

IPC.on('change-watched-input-directory', (event, arg) => {
  currentDirectoryID = arg;
  Dialog.showOpenDialog(openInputDirectoryOptions, onInputDirectoryChange);
})

IPC.on('change-watched-output-directory', (event, arg) => {
  currentDirectoryID = arg;
  Dialog.showOpenDialog(openOutputDirectoryOptions, onOutputDirectoryChange);
})

IPC.on('update-config-compression-compressor', (event, lang, compressor) => {
  currentConfig[lang].compression.compressor = compressor;
  let compression = compressionUtils.selectCompressionByCompressor(config, lang, compressor);
  currentConfig[lang].compression.options = electronUtils.parseConfigOptions(compression.options);
  electronUtils.saveConfig(currentConfig);
})

IPC.on('update-config-compression-compressor-option', (event, lang, optionName, optionValue) => {
  currentConfig[lang].compression.options[optionName] = (optionValue === 'true') ? true : (optionValue === 'false') ? false : optionValue;
  electronUtils.saveConfig(currentConfig);
})

IPC.on('update-config-general', (event, optionName, optionValue) => {
  currentConfig.general[optionName] = (optionValue === 'true') ? true : (optionValue === 'false') ? false : optionValue;
  electronUtils.saveConfig(currentConfig);

  switch (optionName) {
    case "lang":


      let relaunch = Dialog.showMessageBox(
        optionsWindow,
        {
          type: 'question',
          buttons: [localization.messages.YES, localization.messages.NO],
          title: 'Confirm',
          message: localization.messages.CONFIG_RELAUNCH,
          noLink: true,
          icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
        });

      if (relaunch == 0) {
        App.relaunch();
        App.quit();
      }

      break;

  }
})


IPC.on('add-dragged-folder', (event, path) => {
  let paths = [path];
  onDirectoryWatch(paths);
})

IPC.on('add-dragged-folders', (event, paths) => {
  let dirs = [];
  for (let index = 0; index < paths.length; index++) {
    dirs.push(paths[index]);
  }
  onDirectoryWatch(dirs);
})

IPC.on('add-dragged-folders-files', (event) => {
  Dialog.showMessageBox({
    type: "info", message: utils.formatString(localization.messages.MULTIPLE_FILES_FOLDERS_NOT_ALLOWED),
    icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
  });
})

IPC.on('add-dragged-file', (event, path) => {
  let paths = [path];
  let parsedPath = Path.parse(path);
  if (parsedPath.ext.match(/(\.js$)|(\.css$)|(\.scss$)|(\.concat$)/)) {
    onFileOpen(paths);
  } else {
    Dialog.showMessageBox({
      type: "info", message: utils.formatString(localization.messages.INVALID_EXT, { "%EXT%": parsedPath.ext }),
      icon: Path.join(__dirname, 'assets/icons/png/32x32.png')
    });
    mainWindow.webContents.send('add-file-error');
  }

})

IPC.on('error-too-many-files', (event) => {
  Dialog.showErrorBox(localization.messages.PROBLEM_OCCURED, localization.messages.MULTIPLE_FILES_NOT_ALLOWED);
})

/**
 * *******************************************************
 * File watch functions
 * *******************************************************
 */

/**
 * Watches a list of directories and monitors changes
 * @param {*} path 
 */
function watch(path) {
  if (watcher) {
    watcher.close();
    watcher = null;
  }
  let inputDirectories = [];

  for (let index = 0; index < path.length; index++) {
    inputDirectories.push(path[index].input);
  }

  watcher = chokidar.watch(inputDirectories, {
    ignored: [Path.resolve('./node_modules/'), Path.resolve('./.git/'), /.\.min\.js/, /.\.min\.css/, /\_[\w]+\.js/, /\_[\w]+\.css/, /\_[\w]+\.scss/, /\_[\w]+\//]
  });

  // Declare the listeners of the watcher
  watcher
    .on('change', function (changedPath) {
      let parsed = Path.parse(changedPath);
      if (Date.now() - time > minDelayBetweenMinification) {
        setTimeout(() => {
          parseChangedFile(parsed, electronUtils.getLocalStorageDataFromInputDirectory(parsed.dir), false);
        }, 50);
      }
      time = Date.now();
    })
}




function listWatchedDirectories() {
  if (localStorage.getItem("watchedDirectories") != null) {
    let dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
    dirs.forEach(function (element, index) {
      mainWindow.webContents.send('new-watched-directory', element);
    })
    watch(dirs);
  } else console.log(localization.messages.NO_DIR_FOUND);
}


/**
 * *******************************************************
 * Exported functions
 * *******************************************************
 */

function getCurrentConfig() {
  return currentConfig;
}

function getConfigTemplate() {
  return config;
}

module.exports = {
  getCurrentConfig,
  getConfigTemplate,
  currentLang
}
