var config = {
    log: {
        firstRun: true
    },
    general: {
        lang: {
            choices: ["fr", "en"],
            selected: 0
        }

    },
    js: {
        compression: [
            {
                compressor: "uglifyjs",
                options: {
                    warnings: {
                        choices: [true, false],
                        selected: 1
                    },
                    mangle: {
                        choices: [true, false],
                        selected: 0
                    }
                },
            },
            {
                compressor: "butternut",
                options: {},
            }]
    },
    css: {
        compression: [{
            compressor: "yui",
            options: {
                charset: {
                    choices: ["utf8", "ISO-8859-1"],
                    selected: 0
                }
            }
        },
        {
            compressor: "clean-css",
            options: {
                advanced: {
                    choices: [true, false],
                    selected: 0
                },
                aggressiveMerging: {
                    choices: [true, false],
                    selected: 1
                }
            }
        },
        {
            compressor: "csso",
            options: {
                restructureOff: {
                    choices: [true, false],
                    selected: 0
                }
            }
        },
        {
            compressor: "sqwish",
            options: {
                strict: {
                    choices: [true, false],
                    selected: 0
                }
            }
        }]
    },
    scss: {
        compression: [{
            compressor: "node-sass",
            options: {
                outputStyle: {
                    choices: ["nested", "expanded", "compact", "compressed"],
                    selected: 3
                },
                lineFeed: {
                    choices: ["cr", "crlf", "lf", "lfcr"],
                    selected: 1
                }
            }
        }]
    }
}

module.exports = {
    config: config
}