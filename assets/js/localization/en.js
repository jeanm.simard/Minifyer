/**
 * *******************************************************
 * French localization file
 * *******************************************************
 */
module.exports = {
    dialogs: {
        OPEN: "Choose file to load",
        OPEN_CONCAT: "Choose concat file",
        OPEN_DIR: "Choose directory to watch",
        OPEN_INPUT_DIR: "Change input directory",
        OPEN_OUTPUT_DIR: "Change output directory",
    },
    menu: {
        ACTIONS: "Actions",
        ACTIONS_COMPRESS: "Compress a file",
        ACTIONS_CONCAT: "Load a concat file",
        ACTIONS_WATCH: "Watch a directory",
        ACTIONS_QUIT: "Quit",
        PARAMS: "Tools",
        PARAMS_OPTIONS: "Configuration",
        PARAMS_DEV_TOOLS: "DevTools",
        ABOUT: "About",
        ABOUT_DOC: "Documentation",
        ABOUT_VERSION: "Version"
    },
    messages: {
        NO_DIR_FOUND: "No directory found in local storage",
        PROBLEM_OCCURED: "A problem has occured",
        MULTIPLE_FILES_NOT_ALLOWED: "Multiples files are not allowed yet",
        MULTIPLE_FILES_FOLDERS_NOT_ALLOWED: "Files and directories mix is not allowed",
        NO_FILE_SELECTED: "No file selected",
        NO_PATH_SELECTED: "No path selected",
        MINIFY_SUCCESS: "Compressed file saved at :",
        MINIFY_ERROR: "An error occured while compressing the file :",
        SASS_ERROR: "An error occured while compiling the SCSS file",
        SASS_WRITE_ERROR: "An error occured while writing the file",
        SASS_WRITE_SUCCESS: "The SCSS file has been created at :",
        CONCAT_SUCCESS: "The final concat file has been created at :",
        CONCAT_READ_ERROR: "An error occured while reading the concat file",
        CONCAT_ERROR: "An error occured while concatening files",
        INVALID_EXT: "The file extension %EXT% is not valid",
        DIR_ALREADY_WATCHED: "Ther directory %DIR% is already watched",
        CONFIG_RELAUNCH: "You must restart the application for the changes to be taken into account. Do you want to restart now?",
        YES: "Yes",
        NO: "No"
    },
    web: {
        TITLE: "JavaScript, CSS and SCSS minifying",
        INPUT_DIR: "Input directory :",
        INPUT_DIR_CHANGE: "Change input directory for files",
        OUTPUT_DIR: "Output directory :",
        OUTPUT_DIR_CHANGE: "Change output directory for files",
        DELETE_WATCHED_DIR: "Remove this directory from watched list",
        LAST_ACTION_AT: "Last action :",
        CURRENT_ACTION: "Action in progress...",
        OPTIONS: "Options",
        WATCHED_DIRS: "Watched directories",
        DRAG_DROP_HERE: "Drop a file or folders here",
        DRAG_DROP_HERE_PLACEHOLDER: "Drag and drop a file or folders here",
        PARAMETERS: "Parameters",
        GENERAL: "General",
        COMPRESSION: "Minification",
        LANG: "Language"

    }
}