/**
 * *******************************************************
 * French localization file
 * *******************************************************
 */
module.exports = {
    dialogs: {
        OPEN: "Choisir le fichier à charger",
        OPEN_CONCAT: "Choisir le fichier de concaténation",
        OPEN_DIR: "Choisir le dossier à observer",
        OPEN_INPUT_DIR: "Changer le dossier d'entrée",
        OPEN_OUTPUT_DIR: "Changer le dossier de sortie",
    },
    menu: {
        ACTIONS: "Actions",
        ACTIONS_COMPRESS: "Compresser un fichier",
        ACTIONS_CONCAT: "Charger un fichier de concatenation",
        ACTIONS_WATCH: "Observer un dossier",
        ACTIONS_QUIT: "Quitter l'application",
        PARAMS: "Outils",
        PARAMS_OPTIONS: "Configuration",
        PARAMS_DEV_TOOLS: "DevTools",
        ABOUT: "À propos",
        ABOUT_DOC: "Documentation",
        ABOUT_VERSION: "Version"
    },
    messages: {
        NO_DIR_FOUND: "Aucun dossier observés trouvé dans le stockage local",
        PROBLEM_OCCURED: "Un problème est survenu",
        MULTIPLE_FILES_NOT_ALLOWED: "Les fichiers multiples ne sont pas autorisés pour le moment",
        MULTIPLE_FILES_FOLDERS_NOT_ALLOWED: "Le mélange fichiers et dossiers n'est pas accepté",
        NO_FILE_SELECTED: "Aucun fichier sélectionné",
        NO_PATH_SELECTED: "Aucun chemin sélectionné",
        MINIFY_SUCCESS: "Le fichier compressé à été sauvegardé à :",
        MINIFY_ERROR: "Une erreur s'est produite lors de la compression du fichier :",
        SASS_ERROR: "Une erreur s'est produite lors de la compilation du fichier SCSS",
        SASS_WRITE_ERROR: "Une erreur s'est produite lors de l'écriture du fichier",
        SASS_WRITE_SUCCESS: "Le fichier SCSS compilé a été créé à :",
        CONCAT_SUCCESS: "Le fichier concaténé a été créé à :",
        CONCAT_READ_ERROR: "Une erreur s'est produite lors de la lecture du fichier de concaténation.",
        CONCAT_ERROR: "Une erreur s'est produite lors de la concaténation des fichiers.",
        INVALID_EXT: "L'extension %EXT% n'est pas prise en compte.",
        DIR_ALREADY_WATCHED: "Le dossier %DIR% est déjà observé.",
        CONFIG_RELAUNCH: "Vous devez redémarrez afin que les changements soient pris en compte. Voulez-vous redémarrer tout de suite?",
        YES: "Oui",
        NO: "Non"
    },
    web: {
        TITLE: "Compression JavaScript, CSS et SCSS",
        INPUT_DIR: "Dossier d'entrée :",
        INPUT_DIR_CHANGE: "Changer le dossier d'entrée pour les fichiers.",
        OUTPUT_DIR: "Dossier de sortie :",
        OUTPUT_DIR_CHANGE: "Changer le dossier de sortie pour les fichiers.",
        DELETE_WATCHED_DIR: "Retirer ce dossier de la liste d'observation",
        LAST_ACTION_AT: "Dernière action le :",
        CURRENT_ACTION: "Action en cours...",
        OPTIONS: "Options",
        WATCHED_DIRS: "Dossiers observés",
        DRAG_DROP_HERE: "Déposer le fichier ou les dossiers ici",
        DRAG_DROP_HERE_PLACEHOLDER: "Glisser et déposer un fichier ou des dossiers",
        PARAMETERS: "Paramètres",
        GENERAL: "Générales",
        COMPRESSION: "Compression",
        LANG: "Langue"

    }
}