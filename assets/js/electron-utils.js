const URL = require('url');
const Path = require('path');
const Electron = require('electron');
const LocalStorage = require("node-localstorage").LocalStorage;
const localStorage = new LocalStorage('./scratch');
const compressionUtils = require("./compression.js");
const isDev = require('electron-is-dev');

/**
 * 
 * @param {Object} winOptions The options descriptbing the window to create
 * @param {String} url The path of the HTML to load inside the window
 * @param {Function} callback The callback function to cal when done
 * @returns {Electron.BrowserWindow} The newly created window
 */
function newWindow(winOptions, url, callback) {
  // Create the browser window.
  let win = new Electron.BrowserWindow(winOptions)
  if (typeof callback === "function") {
    win.webContents.on('did-finish-load', callback)
  }

  // and load the .html of the window.
  win.loadURL(URL.format({
    pathname: url,
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  win.on('closed', function () {
    win = null
  })

  return win;
}

/**
 * Will craewte the current config object from config template of localstorage
 * @param {Object} config The config template object to use
 * @returns {Object} The newly creasted config object
 */
function createCurrentConfig(config) {
  if (isDev) {
    localStorage.removeItem("config");
  }
  let _config = { js: {}, css: {}, scss: {}, general: {}, log: {} };
  if (localStorage.getItem("config") === null) {
    let compressionJSConfig = compressionUtils.selectCompressionByCompressor(config, "js", "uglifyjs");
    let compressionCSSConfig = compressionUtils.selectCompressionByCompressor(config, "css", "yui");
    let compressionSCSSConfig = compressionUtils.selectCompressionByCompressor(config, "scss", "node-sass");
    _config.js.compression = { compressor: compressionJSConfig.compressor, options: parseConfigOptions(compressionJSConfig.options) }
    _config.css.compression = { compressor: compressionCSSConfig.compressor, options: parseConfigOptions(compressionCSSConfig.options) }
    _config.scss.compression = { compressor: compressionSCSSConfig.compressor, options: parseConfigOptions(compressionSCSSConfig.options) }
    _config.general = parseConfigOptions(config.general);
    _config.log = parseConfigOptions(config.log);
    localStorage.setItem("config", JSON.stringify(_config));
  } else _config = JSON.parse(localStorage.getItem("config"));
  console.log(_config);
  return _config;
}

/**
 * Saves the current config into the localstorage of the app
 * @param {Object} config 
 */
function saveConfig(config) {
  localStorage.setItem("config", JSON.stringify(config));
}

function readCurrentConfig() {
  return JSON.parse(localStorage.getItem("config"));
}

/**
 * Will parse config option from the template choices-selected
 * @param {Object} options The config options to parse
 * @returns {Object} The newly parsed config options
 */
function parseConfigOptions(options) {
  let parsedOptions = {};
  Object.keys(options).forEach(function (key) {
    let option = options[key];
    if (option.choices) {
      parsedOptions[key] = option.choices[option.selected];
    } else {
      parsedOptions[key] = options[key];
    }
  });
  return parsedOptions;
}

/**
 * Updates the localstorage "watched directories" entry
 * @param {String} inputPath The currently known inputh path of the watched directory for which to change value
 * @param {String} key The key of the value to be changed
 * @param {String} value The new value to be saved
 */
function updateLocalStorageWatchedDirectories(inputPath, key, value) {
  let dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
  for (let index = 0; index < dirs.length; index++) {
    if (dirs[index].input == inputPath) {
      dirs[index][key] = value;
      break;
    }
  }
  localStorage.setItem("watchedDirectories", JSON.stringify(dirs));
}

/**
 * Gets the watched directory data from its input path value
 * @param {String} inputDirectory The input path of the directory from which to extract data
 * @returns {null|Object} The data of the directory found or null if not found
 */
function getLocalStorageDataFromInputDirectory(inputDirectory) {
  let dirs = JSON.parse(localStorage.getItem("watchedDirectories"));
  for (let index = 0; index < dirs.length; index++) {
    if (dirs[index].input == inputDirectory) {
      return dirs[index];
      break;
    }
  }
  return null;
}

module.exports = {
  newWindow,
  createCurrentConfig,
  saveConfig,
  readCurrentConfig,
  parseConfigOptions,
  updateLocalStorageWatchedDirectories,
  getLocalStorageDataFromInputDirectory,

}