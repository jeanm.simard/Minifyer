
/**
 * *******************************************************
 * Global fixes
 * *******************************************************
 */
NodeList.prototype.forEach = Array.prototype.forEach;
/**
 * *******************************************************
 * Modules request
 * *******************************************************
 */
const utils = require('../utils.js');
const Path = require('path')
const electron = require('electron');
const IPC = electron.ipcRenderer;
const Remote = electron.remote;
const main = Remote.require("./main.js");
const localization = require("../localization/" + main.currentLang);
let currentCompressionData = { lang: "", compressor: "" };

/**
 * *******************************************************
 * IPC Event callbacks
 * *******************************************************
 */

IPC.on('update-compression-from-config', function (e, currentConfig) {
    let compressionJS = document.querySelector("#options select[name=js]");
    compressionJS.value = currentConfig.js.compression.compressor;
    let compressionCSS = document.querySelector("#options select[name=css]");
    compressionCSS.value = currentConfig.css.compression.compressor;
    let compressionSCSS = document.querySelector("#options select[name=scss]");
    compressionSCSS.value = currentConfig.scss.compression.compressor;

    showGeneralOptions();
});


/**
 * *******************************************************
 * Compression functions
 * *******************************************************
 */


function updateConfigCompressionCompressor(select) {
    IPC.send('update-config-compression-compressor', select.name, select.options[select.selectedIndex].value);
    generateCompressionOptionsFromConfig(select.name, select.options[select.selectedIndex].value, main.getCurrentConfig(), main.getConfigTemplate());
}
function showCompressionOptions(select) {
    generateCompressionOptionsFromConfig(select.name, select.options[select.selectedIndex].value, main.getCurrentConfig(), main.getConfigTemplate());
    currentCompressionData.lang = select.name;
    currentCompressionData.compressor = select.options[select.selectedIndex].value;
}

function updateConfigGeneral(select) {
    let option = select.name.split("!!")[1];
    IPC.send('update-config-general', option, select.options[select.selectedIndex].value);
}

function showGeneralOptions() {
    generateGeneralOptionsFromConfig(main.getCurrentConfig(), main.getConfigTemplate());
}

function updateConfigCompressionCompressorOption(select) {
    IPC.send('update-config-compression-compressor-option', currentCompressionData.lang, select.name.split("!!")[1], select.options[select.selectedIndex].value);
}

function generateGeneralOptionsFromConfig(config, configTemplate) {
    let generalContainer = document.querySelector("#generalOptions");
    generalContainer.innerHTML = "";
    let generalConfig = config.general;
    let generalConfigTemplate = configTemplate.general;
    console.log(generalConfigTemplate);
    for (const key in generalConfigTemplate) {
        if (generalConfigTemplate.hasOwnProperty(key)) {
            const generalOption = generalConfigTemplate[key];

            //title
            console.log(key);
            let optionSubTitle = document.createElement("label");
            optionSubTitle.innerHTML = localization.web[key.toLocaleUpperCase()] + " : ";
            generalContainer.appendChild(optionSubTitle);
            if (generalOption.choices) {
                let optionDiv = document.createElement("div");
                optionDiv.classList.add("select");
                optionDiv.classList.add("is-info");
                optionInput = document.createElement("select");
                optionInput.name = "general!!" + key;
                optionDiv.appendChild(optionInput);
                for (let index = 0; index < generalOption.choices.length; index++) {
                    const choice = generalOption.choices[index];
                    let optionValue = document.createElement("option");
                    optionValue.text = choice;
                    optionValue.value = (choice == "true" || choice == "false") ? Boolean(choice) : choice;
                    optionValue.selected = (generalConfig[key] == choice);
                    optionInput.add(optionValue);
                }
                optionInput.addEventListener("change", function () {
                    updateConfigGeneral(this);
                });
                generalContainer.appendChild(optionDiv);
                generalContainer.appendChild(document.createElement("br"));
            }

        }
    }
}

function generateCompressionOptionsFromConfig(lang, compressor, config, configTemplate) {
    let optionsContainer = document.querySelector("#compressionOptions");
    optionsContainer.innerHTML = "";
    let langCompressionConfig = config[lang].compression;
    let langCompressionConfigTemplate = configTemplate[lang].compression;
    let optionTitle = document.createElement("h3");
    optionTitle.innerText = localization.web.OPTIONS + " " + compressor;
    optionTitle.classList.add("has-text-centered");
    optionTitle.classList.add("title");
    optionTitle.classList.add("is-5");
    optionsContainer.appendChild(optionTitle);
    for (let index = 0; index < langCompressionConfigTemplate.length; index++) {
        if (langCompressionConfigTemplate[index].compressor == compressor) {
            let compressorOptions = langCompressionConfigTemplate[index].options;
            Object.keys(compressorOptions).forEach(function (key) {
                let compressorOption = compressorOptions[key];
                //title
                let optionSubTitle = document.createElement("label");
                optionSubTitle.innerHTML = key + " : ";
                optionsContainer.appendChild(optionSubTitle);
                //value
                let optionValue;
                if (compressorOption.choices) {
                    let optionDiv = document.createElement("div");
                    optionDiv.classList.add("select");
                    optionDiv.classList.add("is-info");
                    optionInput = document.createElement("select");
                    optionInput.name = lang + compressor + "!!" + key;
                    optionDiv.appendChild(optionInput);
                    for (let index = 0; index < compressorOption.choices.length; index++) {
                        const choice = compressorOption.choices[index];
                        let optionValue = document.createElement("option");
                        optionValue.text = choice;
                        optionValue.value = (choice == "true" || choice == "false") ? Boolean(choice) : choice;
                        optionValue.selected = (langCompressionConfig.options[key] == choice);
                        optionInput.add(optionValue);
                    }
                    optionInput.addEventListener("change", function () {
                        updateConfigCompressionCompressorOption(this);
                    });
                    optionsContainer.appendChild(optionDiv);
                    optionsContainer.appendChild(document.createElement("br"));
                } else {
                    optionInput = document.createElement("input");
                    optionInput.value = compressorOption;
                    optionInput.addEventListener("change", function () {
                        updateConfigCompressionCompressorOption(this);
                    });
                    optionsContainer.appendChild(optionInput);
                }
            });
            break;
        }
    }
}

/**
 * *******************************************************
 * Module exports
 * *******************************************************
 */

module.exports = {
    updateConfigCompressionCompressor,
    showCompressionOptions,
    localization
}
