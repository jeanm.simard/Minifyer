
//fixes
NodeList.prototype.forEach = Array.prototype.forEach;
//packages needed
const utils = require('../utils.js');
const Path = require('path')

const electron = require('electron');
const IPC = electron.ipcRenderer;
const Remote = electron.remote;
const main = Remote.require("./main.js");
const localization = require("../localization/" + main.currentLang);
const appVersion = window.require('electron').remote.app.getVersion()

IPC.on('new-watched-directory', function (e, directoryObject) {
    let ul = document.querySelector("#watchedDirectories");
    let currentDirWatched = ul.childNodes.length;

    let li = document.createElement("li");

    li.setAttribute("id", "WD" + (currentDirWatched + 1));
    li.classList.add("box");

    let inputSpan = document.createElement("a");
    inputSpan.innerText = localization.web.INPUT_DIR + " " + directoryObject.input;
    inputSpan.setAttribute("title", localization.web.INPUT_DIR_CHANGE);
    inputSpan.classList.add("inputDirectory");
    inputSpan.doi = directoryObject.input;
    inputSpan.onclick = (e) => {
        IPC.send('change-watched-input-directory', li.id);
    }
    li.appendChild(inputSpan);

    let outputSpan = document.createElement("a");
    outputSpan.innerText = localization.web.OUTPUT_DIR + " " + directoryObject.output;
    outputSpan.setAttribute("title", localization.web.OUTPUT_DIR_CHANGE);
    outputSpan.classList.add("outputDirectory");
    outputSpan.doi = directoryObject.output;
    outputSpan.onclick = (e) => {
        IPC.send('change-watched-output-directory', li.id);
    }
    li.appendChild(outputSpan);

    let btn = document.createElement("a");
    btn.setAttribute("title", localization.web.DELETE_WATCHED_DIR);
    btn.classList.add("delete");
    btn.classList.add("is-large");
    btn.classList.add("is-pulled-right");
    btn.parentDirectory = directoryObject.input;
    btn.onclick = (e) => {
        IPC.send('remove-watched-directory', btn.parentDirectory);
        outputSpan.parentNode.parentNode.removeChild(outputSpan.parentNode);
        if (document.querySelector("#watchedDirectories").childNodes.length == 0) {
            showPlaceholder(true);
        }

    }


    let dateSpan = document.createElement("span");
    dateSpan.innerText = localization.web.LAST_ACTION_AT + " " + directoryObject.lastModificationDate;

    let loading = document.createElement("div");
    loading.classList.add("loading");
    loading.innerText = localization.web.CURRENT_ACTION;

    li.appendChild(btn);
    li.appendChild(dateSpan);
    li.appendChild(loading);
    ul.appendChild(li);

    showPlaceholder(false);
});


IPC.on('change-watched-input-directory-done', function (e, newInputPath, id) {
    let ul = document.querySelector("#watchedDirectories");
    ul.querySelector("#" + id + " .inputDirectory").innerText = localization.web.INPUT_DIR + " " + newInputPath;
});

IPC.on('change-watched-output-directory-done', function (e, newOutputPath, id) {
    let ul = document.querySelector("#watchedDirectories");
    ul.querySelector("#" + id + " .outputDirectory").innerText = localization.web.OUTPUT_DIR + " " + newOutputPath;
});


IPC.on('new-compression', function (e, modificationDate, id, modifiedFilePath) {
    let ul = document.querySelector("#watchedDirectories");
    ul.querySelector("#" + id + " span").innerHTML = localization.web.LAST_ACTION_AT + " " + modificationDate + "<br>" + modifiedFilePath;
    let li = ul.querySelector("#" + id);
    setTimeout(() => {
        li.querySelector(".loading").style.display = "none";
    }, 100);

});

IPC.on('show-loading', function (e, id) {
    let ul = document.querySelector("#watchedDirectories");
    let li = ul.querySelector("#" + id);
    li.querySelector(".loading").style.display = "block";
});

IPC.on('show-options', function (e) {
    let options = document.querySelector("#options");
    options.style.display = "block";
});

IPC.on('loading-done', function (e) {
    let hero = document.querySelector(".hero");

    hero.classList.add("slideup");
});

IPC.on('first-run', function (e) {
    let hero = document.querySelector(".hero");
    hero.classList.remove("isup");
    hero.style.minHeight = "100vh";
});

IPC.on('add-file-error', function (e) {
    if (document.querySelector("#watchedDirectories").childNodes.length == 0) {
        showPlaceholder(true);
    }

    let dragOverlay = document.querySelector("#dropOverlay");
    dragOverlay.style.display = "none";
});

function showPlaceholder(visible) {
    if (visible) {
        let dragPlaceholder = document.querySelector("#dropPlaceholder");
        dragPlaceholder.style.display = "block";

        let titleH3 = document.querySelector(".index h3");
        titleH3.style.display = "none";
    } else {
        let dragPlaceholder = document.querySelector("#dropPlaceholder");
        dragPlaceholder.style.display = "none";

        let titleH3 = document.querySelector(".index h3");
        titleH3.style.display = "block";
    }
}


/**
 * *******************************************************
 * DOM is loaded event
 * *******************************************************
 */
document.addEventListener("DOMContentLoaded", function (event) {
    document.querySelector("#dropOverlay").ondragleave = (ev) => {
        let dragOverlay = document.querySelector("#dropOverlay");
        let dragPlaceholder = document.querySelector("#dropPlaceholder");
        dragOverlay.style.display = "none";
        dragPlaceholder.style.display = "block";
    }
    document.ondragover = (ev) => {
        let dragOverlay = document.querySelector("#dropOverlay");
        let dragPlaceholder = document.querySelector("#dropPlaceholder");
        dragOverlay.style.display = "inline";
        dragPlaceholder.style.display = "none";
        ev.preventDefault()
    }

    document.body.ondrop = (ev) => {

        if (ev.dataTransfer.items.length == 1) {
            let entry = ev.dataTransfer.items[0].webkitGetAsEntry();




            if (entry.isDirectory) {
                //folder
                IPC.send('add-dragged-folder', ev.dataTransfer.files[0].path);
                let dragOverlay = document.querySelector("#dropOverlay");
                dragOverlay.style.display = "none";
                showPlaceholder(false)
            } else {
                //file
                IPC.send('add-dragged-file', ev.dataTransfer.files[0].path);
                let dragOverlay = document.querySelector("#dropOverlay");
                dragOverlay.style.display = "none";
                if (document.querySelector("#watchedDirectories").childNodes.length == 0) {
                    showPlaceholder(true);
                }
            }
        } else {
            let hasFiles = false;
            let hasDirs = false;
            for (let index = 0; index < ev.dataTransfer.items.length; index++) {
                let entry = ev.dataTransfer.items[index].webkitGetAsEntry();
                if (!entry.isDirectory) {
                    hasFiles = true;
                } else if (entry.isDirectory) {
                    hasDirs = true;
                }
            }

            if (hasFiles && !hasDirs) {
                IPC.send('error-too-many-files');
                let dragOverlay = document.querySelector("#dropOverlay");
                dragOverlay.style.display = "none";
                if (document.querySelector("#watchedDirectories").childNodes.length == 0) {
                    showPlaceholder(true);
                }
            } else if (hasDirs && !hasFiles) {
                //multiples directories
                let files = [];
                for (let index = 0; index < ev.dataTransfer.files.length; index++) {
                    const element = ev.dataTransfer.files[index];
                    files.push(element.path);
                }
                IPC.send('add-dragged-folders', files);
                let dragOverlay = document.querySelector("#dropOverlay");
                dragOverlay.style.display = "none";
                showPlaceholder(false)
            } else if (hasDirs && hasFiles) {
                IPC.send('add-dragged-folders-files');
                let dragOverlay = document.querySelector("#dropOverlay");
                dragOverlay.style.display = "none";
                if (document.querySelector("#watchedDirectories").childNodes.length == 0) {
                    showPlaceholder(true);
                }
            }

        }


        ev.preventDefault()
    }



    let version = document.querySelector(".version");
    version.innerText = appVersion;

});
/**
 * *******************************************************
 * Modules exports
 * *******************************************************
 */
module.exports = {
    localization
}