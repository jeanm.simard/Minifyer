const open = require('open');
function openURL (url) {
    open(url);
}

/**
 * *******************************************************
 * Module exports
 * *******************************************************
 */
module.exports = {
    openURL
}