const Compressor = require('node-minify');
const SASS = require('node-sass');

/**
 * Starts the minification process of JavaScript file
 * @param {Object} compressionConfig The compression options to pass to the minifyer
 * @param {String} inputPath The inputh path of the JS file
 * @param {String} outputPath The output path where the compressed file should go
 * @param {Function} callback The callback function to call when done
 */
function minifyJS(compressionConfig, inputPath, outputPath, callback) {
    console.log(compressionConfig);
    Compressor.minify({
        compressor: compressionConfig.compressor,
        input: inputPath,
        output: outputPath,
        options: compressionConfig.options,
        callback: callback
    });
}
/**
 * Starts the minification process of CSS file
 * @param {Object} compressionConfig The compression options to pass to the minifyer
 * @param {String} inputPath The inputh path of the CSS file
 * @param {String} outputPath The output path where the compressed file should go
 * @param {Function} callback The callback function to call when done
 */
function minifyCSS(compressionConfig, inputPath, outputPath, callback) {
    Compressor.minify({
        compressor: compressionConfig.compressor,
        options: compressionConfig.options,
        input: inputPath,
        output: outputPath,
        callback: callback
    });
}

/**
 * Starts the compilation of a SCSS file
 * @param {Object} compressionConfig The compression options to apss to node-sass
 * @param {String} inputPath The inputh path of the SCSS file
 * @param {String} outputPath The output path where the compiled file should go
 * @param {Function} callback The callback function to call when done
 */
function sassify(compressionConfig, inputPath, outputPath, callback) {
    SASS.render({
        file: inputPath,
        outfile: outputPath,
        outputStyle: compressionConfig.options.outputStyle,
        lineFeed: compressionConfig.options.lineFeed
    }, callback);
}

/**
 * Concatenates many files togethere
 * @param {Array} inputFiles An array of file path to concatenate
 * @param {String} outputFile The path for the output file created
 * @param {Function} callback The callback function to call when done
 */
function concat(inputFiles, outputFile, callback) {
    Compressor.minify({
        compressor: 'no-compress',
        input: inputFiles,
        output: outputFile,
        callback: callback
    });
}

/**
 * Return the compression config found from passed argumnents
 * @param {Object} config 
 * @param {String} lang 
 * @param {String} compressor 
 * @returns {null||Object} Ther compression configuration
 */
function selectCompressionByCompressor(config, lang, compressor) {
    let langCompression = config[lang].compression;
    for (let index = 0; index < langCompression.length; index++) {
        let compressionConfig = langCompression[index];
        if (compressionConfig.compressor === compressor) {
            return compressionConfig;
        }
    }
    return null;
}

module.exports = {
    minifyJS,
    minifyCSS,
    sassify,
    concat,
    selectCompressionByCompressor
}