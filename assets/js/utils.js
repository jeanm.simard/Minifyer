
/**
 * Cleans unwanted node type from an HTMLNode
 * @param {HTMLNode} node The parent node for which to clean childrens
 */
function clean(node) {
    for (var n = 0; n < node.childNodes.length; n++) {
        var child = node.childNodes[n];
        if (child.nodeType === 8 || (child.nodeType === 3 && !/\S/.test(child.nodeValue))) {
            node.removeChild(child);
            n--;
        } else if (child.nodeType === 1) {
            clean(child);
        }
    }
}

/**
 * Lazy copy of object
 * @param {*} obj 
 */
function lazyObjectCopy(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function theTime() {
    let currentdate = new Date();
    return currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear() + " @ "
        + currentdate.getHours() + ":"
        + ((currentdate.getMinutes() < 10) ? "0" + currentdate.getMinutes() : currentdate.getMinutes()) + ":"
        + ((currentdate.getSeconds() < 10) ? "0" + currentdate.getSeconds() : currentdate.getSeconds());
}

function formatString(str, replacement) {
    str = str.replace(/%\w+%/g, function (all) {
        return replacement[all] || all;
    });

    return str;
}


module.exports = {
    clean,
    lazyObjectCopy,
    theTime,
    formatString
}